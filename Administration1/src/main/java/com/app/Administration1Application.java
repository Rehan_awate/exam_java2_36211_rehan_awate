package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Administration1Application {

	public static void main(String[] args) {
		SpringApplication.run(Administration1Application.class, args);
	}

}
