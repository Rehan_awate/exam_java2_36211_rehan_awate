package com.app.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Role;
import com.app.pojos.Teacher;
import com.app.service.ITeacherService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	//dependency : Teacher service layer
	@Autowired
	private ITeacherService teacherService;
	public AdminController() {
		System.out.println("in ctor of "+getClass().getName());
	}
	//add request handling method to show Teacher list
	@GetMapping("/list")
	public String showTeacherList(Model map)
	{
		System.out.println("in show Teacher list "+map);
		
		
		map.addAttribute("teacher_list",teacherService.listAllTeacher()); 
		return "/admin/list";
		
	}
	
	@GetMapping("/delete")
	public String removeTeacherDetails(@RequestParam int vid,RedirectAttributes flashMap)
	{
		System.out.println("in remove teacher details "+vid);
		
		flashMap.addFlashAttribute("message", teacherService.deleteTeacherDetails(vid));
		//client pull : redirect scenario
		return "redirect:/admin/list";
	}
	//add req handling method to show vendor registration form
	@GetMapping("/register")
	public String showRegForm(Teacher v)
	{
		System.out.println("in show reg form");
		
		
		return "/admin/register";//actual view name : /WEB-INF/views/admin/register.jsp
	}
	//add req handling method to process vendor registration form
	@PostMapping("/register")
	public String processRegForm(@Valid Teacher v,BindingResult result, RedirectAttributes flashMap)
	
	
	{
		System.out.println("in process reg form "+v);
		v.setUserRole(Role.TEACHER);
	
		if(result.hasErrors()) {
			System.out.println("P.L errs in Data Binding "+result);
		
			return "/admin/register";
			
		}
		System.out.println("No P.L errors ...continuing with B.L");
		flashMap.addFlashAttribute("message",teacherService.registerNewTeacher(v));
		return "redirect:/admin/list";//redirect view name
	}
	
	@GetMapping("/update")
	public String showUpdateForm(@RequestParam int tid,Model map)
	{
		System.out.println("in show update  form "+tid);
		//add retrieved  vendor POJO instance from DB in model map
		map.addAttribute("teacher", teacherService.getTeacherDetails(tid));
		return "/admin/update";//actual view name : /WEB-INF/views/admin/update.jsp
	}
	//add req handling method to process vendor updation form
		@PostMapping("/update")
		public String processUpdateForm(@Valid Teacher t,BindingResult result, RedirectAttributes flashMap)
		
		{
			System.out.println("in process update form "+t);//
			//chk for P.L validation errs 
			if(result.hasErrors()) {
				System.out.println("P.L errs in Data Binding : update "+result);
				
				return "/admin/update";
				
			}
			System.out.println("No P.L errors ...continuing with B.L");
			t.setUserRole(Role.TEACHER);
			flashMap.addFlashAttribute("message",teacherService.updateTeacher(t));
			return "redirect:/admin/list";//redirect view name
		}

}
