package com.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/teacher")
public class TeacherController {
	public TeacherController() {
		System.out.println("in ctor of "+getClass().getName());
	}

	@GetMapping("/details")
	public String showVendorDetails()
	{
		System.out.println("in show Teacher dtls");
		return "/teacher/details";
	}

}
