package com.app.controller;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Role;
import com.app.pojos.Teacher;
import com.app.service.ITeacherService;

@Controller // Mandatory
@RequestMapping("/user") // optional BUT recommended
public class UserController {
	
	@Autowired
	private ITeacherService teacherService;

	public UserController() {
		System.out.println("in ctor of " + getClass().getName() + " " + teacherService);// null

	}

	@PostConstruct
	public void myInit() {
		System.out.println("in user controller's init " + teacherService);// not null
	}

	
	@GetMapping("/login")
	public String showLoginForm() {
		System.out.println("in show login form");
		return "/user/login";// actual view name : /WEB-INF/views/user/login.jsp
	}

	// add req handling method with method=post : to process the form
	@PostMapping("/login")
	public String processLoginForm(@RequestParam String email, @RequestParam String password, Model map,
			HttpSession session, RedirectAttributes flashMap) {
		// RedirectAttributes : i/f => used in redirect scenario, to store the
		// attributes , till the next request
		// String email=request.getParamter("email");...
		System.out.println("in process login form " + email + " " + password);
		// invoke service layer method for exec B.L
		try {
			Teacher validatedUser = teacherService.authenticateUser(email, password);
			// => valid login
			// add validated user details under session scope
			session.setAttribute("user_details", validatedUser);
			// scope : till the next request
			flashMap.addFlashAttribute("message", "Login Successful : " + validatedUser.getUserRole());
			// chk the role n redirect accordingly
			if (validatedUser.getUserRole().equals(Role.ADMIN))
				return "redirect:/admin/list";// redirect view name :
			

			return "redirect:/teacher/details";// to avoid double submit issue : replace server pull by client pull
			// SC :
		} catch (RuntimeException e) {
			System.out.println("err in process login form " + e);// NoResultExc
			
			map.addAttribute("message", "Invalid Login , Pls retry....");
			return "/user/login";// actual view name : /WEB-INF/views/user/login.jsp

		}

	}

	
	@GetMapping("/logout")
	public String userLogout(HttpSession hs, Model modelAttrMap, HttpServletResponse response,
			HttpServletRequest request) {
		System.out.println("in user's logout");
		// get user details from HttpSession n add it the model map
		modelAttrMap.addAttribute("details", hs.getAttribute("user_details"));
		// discard HtpSession
		hs.invalidate();
		
		
		System.out.println("ctx path " + request.getContextPath());//   /day14_boot
		response.setHeader("refresh", "5;url=" + request.getContextPath());
		
		return "/user/logout";
		}
}
