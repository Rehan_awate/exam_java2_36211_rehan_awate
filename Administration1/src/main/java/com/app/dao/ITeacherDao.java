package com.app.dao;

import java.util.List;

import com.app.pojos.Teacher;;

public interface ITeacherDao {
	
	Teacher authenticateUser(String email, String password);

	
	List<Teacher> listAllTeacher();

	
	String deleteTeacherDetails(int teacherId);

	
	String registerNewTeacher(Teacher Teacher);
	
	
	Teacher getTeacherDetails(int teacherId);
	
	
	String updateTeacher(Teacher teacher);
}
