package com.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Role;
import com.app.pojos.Teacher;

@Repository // spring bean : data access logic
public class TeacherDaoImpl implements ITeacherDao {
	// DAO : dependent obj , dependency :
	// javax.persistence.EntityManager (equivalent to org.hibernate.Session)
//	@PersistenceContext
	@Autowired
	private EntityManager manager;

	@Override
	public Teacher authenticateUser(String email, String password) {
		Teacher v = null;
		String jpql = "select v from Teacher v  where v.email=:em and v.password=:pass";

		v = manager.createQuery(jpql, Teacher.class).setParameter("em", email).setParameter("pass", password)
				.getSingleResult();

		return v;
	}

	@Override
	public List<Teacher> listAllTeacher() {
		String jpql = "select v from Teacher v where v.userRole=:role";
		return manager.createQuery(jpql, Teacher.class).setParameter("role", Role.TEACHER).getResultList();
	}

	@Override
	public String deleteTeacherDetails(int teacherId) {
		String mesg="teacher deletion failed";
		
		Teacher v = manager.find(Teacher.class, teacherId);
	
		if (v != null) {
			
			manager.remove(v);
			mesg="Teacher data deleted successfully....";
		}
		return mesg;
	}

	@Override
	public String registerNewTeacher(Teacher teacher) {
		
		manager.persist(teacher); //vendor : PERSISTENT : part of L1 cache 
		return "Teacher registered successfully";
	}

	@Override
	public Teacher getTeacherDetails(int teacherId) {
		// TODO Auto-generated method stub
		return manager.find(Teacher.class, teacherId);
	}

	@Override
	public String updateTeacher(Teacher vendor) {
		manager.merge(vendor);
		return "Teacher details updated...";
	}
	
	
	

}
