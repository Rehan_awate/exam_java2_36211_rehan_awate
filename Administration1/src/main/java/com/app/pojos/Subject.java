package com.app.pojos;


import javax.persistence.*;




@Entity
@Table(name = "tb_subjects")
public class Subject {
	@Id //PK 
	@GeneratedValue(strategy = GenerationType.IDENTITY) //strategy = AUTO will be replaced : auto_increment
	@Column(name = "s_id")
	private Integer subjectNo;

	
	@Column(name="s_name",length = 20)
	private String subjectName;
	
	@Column(name="s_duration",length = 20)
	private double subjectDuration;
	
	
	@Column(name="s_course",length = 20)
	private String subejctCourse;
	
	

	
	//many to one association between entities : owning side
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="t_id",nullable = false)
 private Teacher subjectTeacher;
	
	
	public Subject() {
		System.out.println("in cnstr of "+getClass().getName());
	}

	
	
	
	
	
	
	
	
	
	

	public Subject(Integer subjectNo, String subjectName, double subjectDuration, String subejctCourse) {
		super();
		this.subjectNo = subjectNo;
		this.subjectName = subjectName;
		this.subjectDuration = subjectDuration;
		this.subejctCourse = subejctCourse;
	}












	public Integer getSubjectNo() {
		return subjectNo;
	}


	public void setSubjectNo(Integer subjectNo) {
		this.subjectNo = subjectNo;
	}


	public String getSubjectName() {
		return subjectName;
	}


	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}


	public double getSubjectDuration() {
		return subjectDuration;
	}


	public void setSubjectDuration(double subjectDuration) {
		this.subjectDuration = subjectDuration;
	}


	public String getSubejctCourse() {
		return subejctCourse;
	}


	public void setSubejctCourse(String subejctCourse) {
		this.subejctCourse = subejctCourse;
	}


	


	public Teacher getSubjectTeacher() {
		return subjectTeacher;
	}












	public void setSubjectTeacher(Teacher subjectTeacher) {
		this.subjectTeacher = subjectTeacher;
	}












	@Override
	public String toString() {
		return "Subject [subjectNo=" + subjectNo + ", subjectName=" + subjectName + ", subjectDuration="
				+ subjectDuration + ", subejctCourse=" + subejctCourse + "]";
	}
	
	
	

}
