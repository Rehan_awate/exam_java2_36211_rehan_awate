package com.app.pojos;

//id(Integer),name,email(unique),password,reg amount,reg date (LocalDate),role (enum --vendor / admin)
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "teacher_tbl")
public class Teacher {
	
	
	
	
	
	@Id //PK 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "t_id")
	private Integer teacherId;
	
	@Column(length = 30,name = "t_name")
	@NotBlank(message = "name must supplied")
	@Length(min = 5,max = 20)
	private String name="abc";
	
	@Column(length = 30,unique = true,name = "t_email")
	@NotBlank(message = "Email can't be blank")
	@Email(message = "Invalid Email format")
	private String email;
	
	@Column(length = 30,name = "t_password")
	@Pattern(regexp="((?=.*\\d)(?=.*[a-z])(?=.*[#@$*]).{5,10})",message = "Blank Or Invalid Password ")
	private String password;
	
	@Column(name="t_roll")
	@Min(100)
	@Max(500)
	private Integer rollno;
	
	@Column(name = "t_dob")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Past
	private LocalDate DateofBirth;//col type=date
	
	@Enumerated(EnumType.STRING)
	@Column(name="user_role",length = 20)
	private Role userRole;
	
	
	@OneToMany(mappedBy = "subjectTeacher",cascade = CascadeType.ALL,orphanRemoval = true)
	private List<Subject> subjects=new ArrayList<>();

	
	
	
	
	public Teacher() {
		System.out.println("ctor of teacher");
	}
	
	
	
	
	
	
	
	

	public Teacher(Integer teacherId,  String name,
			 String email,
			 String password,
			 Integer rollno, LocalDate dateofBirth,
			 Role userRole) {
		super();
		this.teacherId = teacherId;
		this.name = name;
		this.email = email;
		this.password = password;
		this.rollno = rollno;
		DateofBirth = dateofBirth;
		this.userRole = userRole;
	}









	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getRollno() {
		return rollno;
	}

	public void setRollno(Integer rollno) {
		this.rollno = rollno;
	}

	public LocalDate getDateofBirth() {
		return DateofBirth;
	}

	public void setDateofBirth(LocalDate dateofBirth) {
		DateofBirth = dateofBirth;
	}

	
	
	



	public List<Subject> getSubjects() {
		return subjects;
	}


	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}






	public void addSubject(Subject a)
	{
		subjects.add(a);
		a.setSubjectTeacher(this);
	}
	public void removeAccount(Subject a)
	{
		subjects.remove(a);
		a.setSubjectTeacher(null);
	}







	









	public Role getUserRole() {
		return userRole;
	}









	public void setUserRole(Role userRole) {
		this.userRole = userRole;
	}









	@Override
	public String toString() {
		return "Teacher [teacherId=" + teacherId + ", name=" + name + ", email=" + email + ", password=" + password
				+ ", rollno=" + rollno + ", DateofBirth=" + DateofBirth + ", userRole=" + userRole + "]";
	}









	
	
	
	

}
