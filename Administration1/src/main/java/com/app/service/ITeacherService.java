package com.app.service;

import java.util.List;

import com.app.pojos.Teacher;

public interface ITeacherService {
	Teacher authenticateUser(String email, String password);

	// add a method to list 
	List<Teacher> listAllTeacher();

	// add a method to delete  details
	String deleteTeacherDetails(int teacherId);

	// add a method to register new Teacher
	String registerNewTeacher(Teacher teacher);

	// add a method to fetchTeacher details
	Teacher getTeacherDetails(int teacherId);

	// add a method to update Teacher details
	String updateTeacher(Teacher teacher);
}
