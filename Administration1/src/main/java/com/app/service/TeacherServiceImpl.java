package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.ITeacherDao;
import com.app.pojos.Teacher;

@Service //Mandatory annotation to tell SC whatever follows contains B.L
@Transactional//Mandatory : annotation to tell SC , to use tx mgr bean for
//automatically handling txs.
public class TeacherServiceImpl implements ITeacherService {
	//dependency : DAO layer
	@Autowired
	private ITeacherDao teacherDao;

	@Override
	public Teacher authenticateUser(String email, String password) {
		// simply invoke dao's method for user authentication
		return teacherDao.authenticateUser(email, password);
	}

	@Override
	public List<Teacher> listAllTeacher() {
		// TODO Auto-generated method stub
		return teacherDao.listAllTeacher();
	}

	@Override
	public String deleteTeacherDetails(int teacherId) {
		// TODO Auto-generated method stub
		return teacherDao.deleteTeacherDetails(teacherId);
	}

	@Override
	public String registerNewTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		return teacherDao.registerNewTeacher(teacher);
	}

	@Override
	public Teacher getTeacherDetails(int teacherId) {
		// TODO Auto-generated method stub
		return teacherDao.getTeacherDetails(teacherId);
	}

	@Override
	public String updateTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		return teacherDao.updateTeacher(teacher);
	}
		
	

}
