<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>

  <script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
        <script src="/webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
       
        <link rel="stylesheet" 
          href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" />


<meta charset="UTF-8">
<title>Teacher's List</title>

<Style>


</Style>

</head>
<body>
<div class="container col-md-4">
	<h5>${requestScope.message}</h5>
	<h5>Admin's details : ${sessionScope.user_details}</h5>

	<table class="table table-bordered table-dark" style=" margin: auto; top:0;bottom:0;right:0;left:0;"border="1" >
		<caption>Teacher List</caption>
		
		<thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">RollNo</th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
  </thead>
		<c:forEach var="v" items="${requestScope.teacher_list}">
			<tr>
				<td>${v.name}</td>
				<td>${v.email}</td>
				<td>${v.rollno}</td>
			
				<td><a
					href="<spring:url value='/admin/update?tid=${v.teacherId}'/>">Update</a></td>
				<td><a
					href="<spring:url value='/admin/delete?tid=${v.teacherId}'/>">Delete</a></td>

				<td></td>
			</tr>
		</c:forEach>
	</table>
	
	
	
	
	
	
	
	<h5>
		<a href="<spring:url  value='/admin/register'/>">Add New teacher Details</a>
	</h5>
	<h5>
		<a href="<spring:url value='/user/login'/>">Log Me Out</a>
	</h5>
	
	</div>
</body>
</html>