<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%--to enable form binding : import spring supplied form tag lib --%>
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<form:form method="post" modelAttribute="teacher">
	<table class="table table-bordered table-dark" style=" margin: auto; top:0;bottom:0;right:0;left:0;"border="1" >
		
			<tr>
				<td>Enter Teacher Email</td>
				<td><form:input type="email" path="email" /></td>
				<td><form:errors path="email"/></td>
			</tr>
			<tr>
				<td>Enter Teacher Name</td>
				<td><form:input  path="name" /></td>
				<td><form:errors path="name"/></td>
			</tr>
			<tr>
				<td>Enter Password</td>
				<td><form:password  path="password" /></td>
				<td><form:errors path="password"/></td>
			</tr>
			<tr>
				<td>Enter Roll</td>
				<td><form:input type="number" path="rollno" /></td>
				<td><form:errors path="rollno"/></td>
			</tr>
			<tr>
				<td>Choose Reg Date</td>
				<td><form:input type="date" path="DateofBirth" /></td>
				<td><form:errors path="DateofBirth"/></td>
			</tr>

			<tr>
				<td><input type="submit" value="Register New Vendor" /></td>
			</tr>
		</table>
	</form:form>

</body>
</html>