<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%--  importing spring supplied JSP tag lib --%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
        <script src="/webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
       
        <link rel="stylesheet" 
          href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" />
    </head>
    <style>
    
    </style>
    <body>
    
    
    
    	<%--URL rewriting if needed n makes all URI relative to root of curnt web app  --%>
	<h5>
		<a href="<spring:url value='/user/login'/>">User Login</a>
	</h5>
    
    
 

    </body>
</html>