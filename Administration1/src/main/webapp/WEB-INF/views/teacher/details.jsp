<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Teacher's Details</title>
</head>
<body>
	<h5>${requestScope.message}</h5>
	<h5>Teacher's details : ${sessionScope.user_details}</h5>
	<%--add logout link --%>
	<h5>
		<a href="<spring:url value='/user/login'/>">Log Me Out</a>
	</h5>
	<h5>
	<a href="<spring:url value='/admin/update?tid=${v.teacherId}'/>">Update</a>
	</h5>
</body>
</html>