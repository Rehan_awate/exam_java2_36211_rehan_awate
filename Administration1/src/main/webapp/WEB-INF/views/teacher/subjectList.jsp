<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>

  <script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
        <script src="/webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
       
        <link rel="stylesheet" 
          href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" />


<meta charset="UTF-8">
<title>subjects List</title>

<Style>


</Style>

</head>
<body>
<div class="container col-md-4">
	<h5>${requestScope.message}</h5>
	

	<table class="table table-bordered table-dark" style=" margin: auto; top:0;bottom:0;right:0;left:0;"border="1" >
		<caption>subjects List</caption>
		
		<thead>
    <tr>
      <th scope="col">subject Course</th>
      <th scope="col">subject Duration</th>
      <th scope="col">subject Name</th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
  </thead>
		<c:forEach var="v" items="${requestScope.subject_list}">
			<tr>
				<td>${v.subjectCourse}</td>
				<td>${v.subjectDuration}</td>
				<td>${v.subjectName}</td>
			
				<td><a
					href="<spring:url value='/subject/add?tid=${v.subjectId}'/>">Add Subject</a>
					</td>
				
			</tr>
		</c:forEach>
	</table>
	
	</h5>
	
	</div>
</body>
</html>