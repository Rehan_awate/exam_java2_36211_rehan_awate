<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%--to enable form binding : import spring supplied form tag lib --%>
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<form:form method="post" modelAttribute="teacher">
			<table class="table table-bordered table-dark" style=" margin: auto; top:0;bottom:0;right:0;left:0;"border="1" >
		
		<tr>
		<td>Vendor ID (R)</td>
		<td><form:input  path="teacherId" readonly="true"/></td>
		</tr>
		
			<tr>
				<td>Enter Vendor Name</td>
				<td><form:input  path="name" /></td>
				<td><form:errors path="name"/></td>
			</tr>
			<tr>
				<td>Enter Password</td>
				<td><form:password  path="password" /></td>
				<td><form:errors path="password"/></td>
			</tr>
		

			<tr>
				<td><input type="submit" value="Update Details" /></td>
			</tr>
		</table>
	</form:form>

</body>
</html>